<?php
/**
 * Elgg Admin Area Canvas
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['content'] Content string
 * @uses $vars['sidebar'] Optional sidebar content
 * @uses $vars['title']   Title string
 * 
 * @override views/default/page/layouts/admin.php
 */

?>

<div class="elgg-layout elgg-layout-one-sidebar">
	<aside class="elgg-sidebar clearfix">
		<?php
			echo elgg_view('admin/sidebar', $vars);
		?>
	</aside>
	<div class="elgg-main elgg-body">
		<div class="elgg-head">
		<?php
			echo elgg_view_menu('title', array(
				'sort_by' => 'priority',
				'class' => 'elgg-menu-hz',
			));

			if (isset($vars['title'])) {
				echo elgg_view_title($vars['title']);
			}
		?>
		</div>
		<?php
			if (isset($vars['content'])) {
				echo $vars['content'];
			}
		?>
	</div>
</div>